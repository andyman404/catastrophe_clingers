%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: ArmMask
  m_Mask: 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/hips
    m_Weight: 0
  - m_Path: Armature/hips/butt
    m_Weight: 0
  - m_Path: Armature/hips/butt/butt_end
    m_Weight: 0
  - m_Path: Armature/hips/IK_hand.L
    m_Weight: 0
  - m_Path: Armature/hips/IK_hand.L/IK_hand.L_end
    m_Weight: 0
  - m_Path: Armature/hips/IK_hand.R
    m_Weight: 0
  - m_Path: Armature/hips/IK_hand.R/IK_hand.R_end
    m_Weight: 0
  - m_Path: Armature/hips/spine
    m_Weight: 0
  - m_Path: Armature/hips/spine/belly
    m_Weight: 0
  - m_Path: Armature/hips/spine/belly/belly_end
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/boob
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/boob/boob_end
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/neck
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/neck/head
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/neck/head/lower_jaw
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/neck/head/lower_jaw/lower_jaw_end
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/neck/head/upper_face
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/neck/head/upper_face/upper_face_end
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/neck/head/upper_jaw
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/neck/head/upper_jaw/upper_jaw_end
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/shoulder.L
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/shoulder.L/upper_arm.L
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/shoulder.L/upper_arm.L/forearm.L
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/shoulder.L/upper_arm.L/forearm.L/hand.L
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/shoulder.L/upper_arm.L/forearm.L/hand.L/hand.L_end
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/shoulder.R
    m_Weight: 0
  - m_Path: Armature/hips/spine/torso/chest/shoulder.R/upper_arm.R
    m_Weight: 1
  - m_Path: Armature/hips/spine/torso/chest/shoulder.R/upper_arm.R/forearm.R
    m_Weight: 1
  - m_Path: Armature/hips/spine/torso/chest/shoulder.R/upper_arm.R/forearm.R/hand.R
    m_Weight: 1
  - m_Path: Armature/hips/spine/torso/chest/shoulder.R/upper_arm.R/forearm.R/hand.R/hand.R_end
    m_Weight: 1
  - m_Path: Armature/hips/thigh.L
    m_Weight: 0
  - m_Path: Armature/hips/thigh.L/shin.L
    m_Weight: 0
  - m_Path: Armature/hips/thigh.L/shin.L/foot.L
    m_Weight: 0
  - m_Path: Armature/hips/thigh.L/shin.L/foot.L/foot.L_end
    m_Weight: 0
  - m_Path: Armature/hips/thigh.R
    m_Weight: 0
  - m_Path: Armature/hips/thigh.R/shin.R
    m_Weight: 0
  - m_Path: Armature/hips/thigh.R/shin.R/foot.R
    m_Weight: 0
  - m_Path: Armature/hips/thigh.R/shin.R/foot.R/foot.R_end
    m_Weight: 0
  - m_Path: Armature/IK_elbow.L
    m_Weight: 0
  - m_Path: Armature/IK_elbow.L/IK_elbow.L_end
    m_Weight: 0
  - m_Path: Armature/IK_elbow.R
    m_Weight: 0
  - m_Path: Armature/IK_elbow.R/IK_elbow.R_end
    m_Weight: 0
  - m_Path: Armature/IK_foot.L
    m_Weight: 0
  - m_Path: Armature/IK_foot.L/IK_foot.L_end
    m_Weight: 0
  - m_Path: Armature/IK_foot.R
    m_Weight: 0
  - m_Path: Armature/IK_foot.R/IK_foot.R_end
    m_Weight: 0
  - m_Path: Armature/IK_knee.L
    m_Weight: 0
  - m_Path: Armature/IK_knee.L/IK_knee.L_end
    m_Weight: 0
  - m_Path: Armature/IK_knee.R
    m_Weight: 0
  - m_Path: Armature/IK_knee.R/IK_knee.R_end
    m_Weight: 0
  - m_Path: catlady
    m_Weight: 0
  - m_Path: hair
    m_Weight: 0
