﻿Shader "Custom/Janglifier"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
		_Seed("Seed", Vector) = (0.0, 0.0, 0.0, 0.0)
		_Jangliness("Jangliness", Range(0.0, 1.0)) = 0.0
		_JangledColor("JangledColor", Color) = (0.5, 0.5, 0.5, 1.0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard vertex:vert fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
		fixed4 _JangledColor;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
			UNITY_DEFINE_INSTANCED_PROP(fixed4, _Color)
			UNITY_DEFINE_INSTANCED_PROP(float4, _Seed)
			UNITY_DEFINE_INSTANCED_PROP(float, _Jangliness)

            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

			float hash(float n)
		{
			return frac(sin(n) * 43758.5453);
		}

		float noise1(float3 x)
		{
			x.y = 0.0;

			// The noise function returns a value in the range 0 -> 1.0f

			float3 p = floor(x);
			float3 f = frac(x);

			f = f * f * (float3(3.0, 3.0, 3.0) - f * 2.0);
			float n = p.x + p.y * 57.0 + 113.0 * p.z;


			return((lerp(lerp(lerp(hash(n + 0.0), hash(n + 1.0), f.x),
				lerp(hash(n + 57.0), hash(n + 58.0), f.x), f.y),
				lerp(lerp(hash(n + 113.0), hash(n + 114.0), f.x),
					lerp(hash(n + 170.0), hash(n + 171.0), f.x), f.y), f.z)));

		}

		float4 jangleIt(float3 v, float jangle, float4 seed)
		{
			// convert to world
			float4 worldV = mul(unity_ObjectToWorld, v);

			// jangle it
			
			worldV.x += sin(v.x * 20000.0 + v.z*10100.0 + v.y * 83043.0+ seed.x) * jangle * 0.1;
			worldV.y += sin(v.x * 20400.0 + v.z * 42343.0 + v.y * 8308.0 + seed.y) * jangle * 0.1;
			worldV.z += sin(v.x * 54355.0 + v.z * 10100.0 + v.y * 54773.0 + seed.z) * jangle * 0.1;

			// convrt to local
			return mul(unity_WorldToObject, worldV);
		}

		void vert(inout appdata_full v) {
			float jangle = UNITY_ACCESS_INSTANCED_PROP(Props, _Jangliness);
			float4 seed = UNITY_ACCESS_INSTANCED_PROP(Props, _Seed);
			v.vertex = jangleIt(v.vertex, jangle, seed);
		}
        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			float jangle = UNITY_ACCESS_INSTANCED_PROP(Props, _Jangliness);

			fixed4 color = UNITY_ACCESS_INSTANCED_PROP(Props, _Color);
			fixed4 jangleColor = _JangledColor;// UNITY_ACCESS_INSTANCED_PROP(Props, _JangledColor);

            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * lerp(color, jangleColor, jangle);

            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
