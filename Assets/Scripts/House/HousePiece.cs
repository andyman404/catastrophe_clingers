﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HousePiece : MonoBehaviour
{
	public float jointMargin = 0.25f;
	public List<FixedJoint> joints = new List<FixedJoint>(8);

	public float jointStrength = 4000.0f;
	public float jointTorqueStrength = 4000.0f;
	private static Collider[] outResults = new Collider[128];
	public bool shouldAutoJoinOnStart = true;

	public LayerMask joinLayerMask;
	private static HashSet<GameObject> connectedObjectsScratch = new HashSet<GameObject>();
	public static HashSet<HousePiece> allPieces = new HashSet<HousePiece>();

	private MaterialPropertyBlock mpb;
	private MeshRenderer mr;

	public float jangliness = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
		mr = GetComponent<MeshRenderer>();
		AutoJoin();
		mpb = new MaterialPropertyBlock();
		mpb.SetVector("_Seed", new Vector4(Random.value*10000.0f, Random.value * 10000.0f, Random.value * 10000.0f, Random.value * 10000.0f));
		SetJangle(jangliness);
		FlushMeshPropertyBlock();
    }

	// range of 0 to 1
	private void SetJangle(float amount)
	{
		jangliness = amount;
		jangliness = Mathf.Clamp(jangliness, 0.0f, 1.0f);
		mpb.SetFloat("_Jangliness", amount);
	}
	public bool emitParticles = false;
	private float nextEmitTime = 0.0f;

	private void OnJointBreak(float breakForce)
	{
		if (shouldAutoJoinOnStart)
		{
			SetJangle(jangliness + Random.value * (1.0f / ((float)joints.Count)));
			FlushMeshPropertyBlock();

			if (emitParticles /*&& Time.time < nextEmitTime*/)
			{
				LevelController.instance.dustParticleSystem.transform.position = transform.position;
				LevelController.instance.dustParticleSystem.Emit(Random.Range(1, 5));
				//nextEmitTime = Time.time + Random.Range(3.0f, 6.0f);
				LevelController.instance.debrisParticleSystem.transform.position = transform.position;
				LevelController.instance.debrisParticleSystem.Emit(Random.Range(1, 2));
				//nextEmitTime = Time.time + Random.Range(3.0f, 6.0f);
			}
		}
	}

	private void FlushMeshPropertyBlock()
	{
		if (mr != null)
		{
			mr.SetPropertyBlock(mpb);
		}
	}
	private Bounds autoBounds;
	void AutoJoin()
	{
		Collider myCollider = GetComponent<Collider>();
		Bounds bounds = myCollider.bounds;
		bounds.extents = bounds.extents + Vector3.one * jointMargin;
		autoBounds = bounds;

		// get everything in range of the bounds and joint it if it is not already jointed


		// clear all managed joints first
		for(int i = 0; i < joints.Count; i++)
		{
			FixedJoint joint = joints[i];
			if (joint != null)
			{
				Destroy(joint);
			}
		}
		joints.Clear();
		int nearbyCount = Physics.OverlapBoxNonAlloc(bounds.center, bounds.extents, outResults, Quaternion.identity, joinLayerMask);

		connectedObjectsScratch.Clear();
		// connect to nearby joints
		for (int i = 0; i < nearbyCount; i++)
		{
			Collider otherCollider = outResults[i];
			Rigidbody otherRb = otherCollider.GetComponent<Rigidbody>();
			if (otherRb != null && otherRb.gameObject != gameObject)
			{
				if (!connectedObjectsScratch.Contains(otherRb.gameObject))
				{
					FixedJoint joint = gameObject.AddComponent<FixedJoint>();
					joint.connectedBody = otherRb;
					joint.enableCollision = false;
					joint.breakForce = jointStrength;
					joint.breakTorque = jointTorqueStrength;
					joints.Add(joint);
					connectedObjectsScratch.Add(otherRb.gameObject);

				}
			}
		}

	}

	void OnEnable()
	{
		allPieces.Add(this);
	}
	void OnDisable()
	{
		allPieces.Remove(this);
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnDrawGizmosSelected()	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireCube(autoBounds.center, autoBounds.size);
	}
}
