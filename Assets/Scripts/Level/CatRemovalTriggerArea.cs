﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatRemovalTriggerArea : MonoBehaviour
{

	private void OnTriggerStay(Collider other)
	{
		OnTriggerEnter(other);
	}
	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer != Layers.CATS)
			return;

		Rigidbody rb = other.attachedRigidbody;
		if (rb == null) return;
		CatController cat = rb.GetComponent<CatController>();
		if (cat == null) return;

		if (!cat.shrinkMode)
		{
			cat.shrinkMode = true;
			Destroy(cat, 6.0f);
		}
	}
}
