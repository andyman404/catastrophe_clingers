﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


// the dump script for the level and win/loss conditions
public class LevelController : MonoBehaviour
{
	public static LevelController instance = null;

	public bool isGameOver = false;
	public ParticleSystem dustParticleSystem;
	public ParticleSystem debrisParticleSystem;
	public PlayerController player;
	public earthquake quaker;
	public UnityEvent playerWonEvent;
	public UnityEvent overallGameOverEvent;
	public int level = 1;
	public Text subtitleText;
	public Text daysLabel;
	void OnEnable()
	{
		if (instance == null)
		{
			instance = this;
		}
	}
	
	void OnDisable()
	{
		if (instance == this)
		{
			instance = null;
		}
	}

	public IEnumerator LevelCoroutine()
	{
		yield return 0;

		float quakeTime = Time.time + 15.0f;

		WaitForSeconds waiter = new WaitForSeconds(0.1f);

		subtitleText.enabled = false;
		daysLabel.enabled = true;
		daysLabel.text = "DAY " + level;

		yield return new WaitForSeconds(3.0f);

		daysLabel.text = "";
		daysLabel.enabled = false;
		yield return new WaitForSeconds(3.0f);

		subtitleText.enabled = true;

		while (Time.time < quakeTime)
		{
			int timeLeft = Mathf.CeilToInt(quakeTime - Time.time);

			subtitleText.text = timeLeft.ToString() +" SECONDS UNTIL EARTHQUAKE";
			yield return waiter;
		}


		// start earthquake
		subtitleText.enabled = true;

		subtitleText.text = "<size=80><color=red>EARTHQUAKE!</color></size>";
		quaker.trig = true;
		quaker.intensity = ((float)level) * 0.25f;

		yield return new WaitForSeconds(1.5f);
		quaker.trig = false;

		subtitleText.text = "OH DEAR!!!";

		yield return new WaitForSeconds(1.5f);

		subtitleText.text = "REPAIR YOUR HOUSE BEFORE THE NEXT EARTHQUAKE";

		yield return new WaitForSeconds(8.0f);

		subtitleText.text = "";

		level++;

		StartLevelConroutine();
	}

	public void StartLevelConroutine()
	{
		StartCoroutine(LevelCoroutine());
	}

    // Start is called before the first frame update
    private IEnumerator Start()
    {
		yield return 0;
		float endTime = Time.time + 2.0f;
		float startTime = Time.time;
		while(Time.time < endTime)
		{
			yield return 0;
			Physics.sleepThreshold = Mathf.InverseLerp(startTime, endTime, Time.time) * 200.0f;
		}
        
    }

    // Update is called once per frame
    void Update()
    {
		if (!isGameOver)
		{
			UpdatePlay();
		}
		else if (isGameOver)
		{
			UpdateGameOver();
		}
    }

	void UpdatePlay()
	{

	}


	void UpdateGameOver()
	{

	}
	
	public void QuitGame()
	{
		Application.Quit();
	}
	public void RestartGame()
	{
		Time.timeScale = 1.0f;

		SceneManager.LoadScene(0);
	}
}
