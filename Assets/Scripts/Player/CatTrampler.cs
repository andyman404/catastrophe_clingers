﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// class to add to the player to cause cats to attack you if you touch a clinging cat
public class CatTrampler : MonoBehaviour
{

	private void OnCollisionEnter(Collision collision)
	{
		OnCollisionStay(collision);
	}

	private void OnCollisionStay(Collision collision)
	{
		// check to see if come in contact with cat
		if (collision.gameObject.layer != Layers.CATS)
			return;

		Rigidbody catRb = collision.collider.attachedRigidbody;
		if (catRb == null) return;

		CatController cat = catRb.GetComponent<CatController>();
		if (cat == null) return;

		cat.Attack(transform);
	}
}
