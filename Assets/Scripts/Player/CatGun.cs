﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatGun : MonoBehaviour
{
    public CatController catPrefab;
    public Transform shootOrigin;
    public float projectileSpeed = 20.0f;
    public Camera mainCamera;
    public PlayerController player;
    public LayerMask targetLayerMask;
    public Animator anim;
    private float nextEaseTime = 0.0f;
    public AudioClip gunClip;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
		// don't fire if paused
		if (Time.timeScale == 0.0f)
		{
			return;
		}

        if(Input.GetButtonDown("Fire1"))
        {
            Fire();
        }

        if (Time.time > nextEaseTime)
        {
            float layerWeight = anim.GetLayerWeight(3);
            layerWeight = Mathf.Lerp(layerWeight, 0.0f, Time.time * 1.0f);
            anim.SetLayerWeight(3, layerWeight);
        }
    }

    void Fire()
    {
        ProcAudioSource.Play(gunClip, transform.position, Random.Range(0.5f, 0.7f), Random.Range(0.8f, 1.2f));

        anim.SetLayerWeight(3, 1.0f);
        nextEaseTime = Time.time + 3.0f;
        // find what direction mouse is pointing
        // and project it along the plane of the character's z

        Vector3 playerPos = player.transform.position;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        Vector3 shootPoint = playerPos + Vector3.up;
        Vector3 hitPoint;

        if (Physics.Raycast(ray, out hit, 100.0f, targetLayerMask))
        {
            hitPoint = hit.point;

        }
        else
        {
            hitPoint = ray.GetPoint(100.0f);
        }

        Vector3 playerFacing = (hitPoint - shootPoint).normalized;

        Quaternion startRotation = Quaternion.LookRotation(playerFacing);
        Vector3 startPoint = shootPoint + startRotation * Vector3.forward;

        playerFacing.y = 0.0f;
        playerFacing.Normalize();

        if (Vector3.Dot(playerFacing, player.transform.forward) < 0.0f)
        {
            player.transform.rotation = Quaternion.LookRotation(playerFacing);
        }

        CatController cat = Instantiate<CatController>(catPrefab, startPoint, startRotation);
        cat.transform.localScale = Vector3.one * Random.Range(0.75f, 1.5f);
        cat.rb.velocity = player.rb.velocity + startRotation * Vector3.forward * projectileSpeed;
        cat.rb.angularVelocity = new Vector3(
            Random.Range(-1.0f, 1.0f) * 720.0f,
            Random.Range(-1.0f, 1.0f) * 720.0f,
            Random.Range(-1.0f, 1.0f) * 720.0f
            );
        anim.SetTrigger("fire");


    }
}
