﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
	public float speed = 4.0f;
	public float jumpSpeed = 8.0f;
	public LayerMask groundedLayerMask;
	public Rigidbody rb;

	public Transform footCenter;
	public float groundCheckRadius;

	public Animator anim;
	public bool grounded;

	private bool jumpPressed = false;
	public Vector2 inputs;

	private float nextJumpAvailable = 0;

	public Damageable damageable;

    // Start is called before the first frame update
    void Start()
    {
		rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
		inputs.x = Input.GetAxis("Horizontal");
		inputs.y = Input.GetAxis("Vertical");
		jumpPressed = Input.GetButton("Jump");
		anim.SetBool("grounded", grounded);
    }

	void FixedUpdate()
	{
		grounded = Physics.CheckSphere(footCenter.position, groundCheckRadius, groundedLayerMask);

		Vector3 v = rb.velocity;

		Vector2 flatV = inputs * speed;
		v.x = flatV.x;
		v.z = flatV.y;
		if (jumpPressed && grounded && Time.time >= nextJumpAvailable)
		{

			v.y = jumpSpeed;
			grounded = false;
			nextJumpAvailable = Time.time + 0.1f;
		}
		jumpPressed = false;

		// rotation in direction if we're moving
		if (inputs.magnitude > 0.1f)
		{
			Vector3 flatDir = v;
			flatDir.y = 0.0f;
			flatDir.Normalize();
			Quaternion targetRotation = Quaternion.LookRotation(flatDir);
			Quaternion currentRotation = rb.rotation;
			Quaternion smoothRotation = Quaternion.Slerp(currentRotation, targetRotation, Time.deltaTime * 5.0f);
			rb.MoveRotation(smoothRotation);
		}

		if (!grounded)
		{
			v.y += Physics.gravity.y * 2.0f * Time.deltaTime;
		}

		// speed limit
		if (v.magnitude > (jumpSpeed+speed))
		{
			v = (v.normalized * (jumpSpeed + speed));
		}

		rb.velocity = v;

		// tODO facing

	}
}
