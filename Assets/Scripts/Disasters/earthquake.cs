﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class earthquake : MonoBehaviour
{
	public Rigidbody rb;

    public bool trig = false;
    public float intensity = 0.25f;

    private Vector3 ogpos;
    private float nextShakeTime = 0.0f;
	private bool started = false;

	public void Awake()
	{
		rb = GetComponent<Rigidbody>();
	}
	public void SetIntensity(float val)
	{
		intensity = val;
	}
	public void SetTrig(bool val)
	{
		trig = val; 
	}
    private IEnumerator Start()
    {
		yield return new WaitForSeconds(1f);
        ogpos = this.transform.position;
		started = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (started && trig && Time.time > nextShakeTime)
        {
            Vector3 newoffset = new Vector3(Random.Range(-intensity, intensity), 0f, Random.Range(-intensity, intensity));
            //transform.position = (ogpos + newoffset);
			rb.MovePosition(ogpos + newoffset);
            nextShakeTime = Time.time + Random.Range(0.1f, 0.5f);
        }
    }
}
