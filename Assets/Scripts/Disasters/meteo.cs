﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meteo : MonoBehaviour
{
    public GameObject meteor;
    public bool sp;
    public Transform target;
    public Transform spawnloc;

    private void Start()
    {
        spawnloc = this.transform;
        spawnloc.position = target.transform.position + new Vector3(0.0f, 25.0f, 0.0f);
    }
    // Update is called once per frame
    void Update()
    {
        if (sp)
        {
            Instantiate(meteor, spawnloc.position, spawnloc.rotation);
            sp = false;
        }
    }
}
