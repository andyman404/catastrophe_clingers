﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jointlabels : MonoBehaviour
{
    
    private void OnDrawGizmos()
    {
        FixedJoint fj = GetComponent<FixedJoint>();
        if (GetComponent<FixedJoint>())
        {
            Gizmos.color = Color.blue;
            if (fj.connectedBody)
            {
                Gizmos.DrawLine(GetComponent<FixedJoint>().transform.position, GetComponent<FixedJoint>().connectedBody.transform.position);
            }
            
        }
    }
}
