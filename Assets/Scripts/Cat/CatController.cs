﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CatState
{
    Projectile = 0,
    Clinging = 1,
    Chasing = 2,
	Attacking = 3
}
public class CatController : MonoBehaviour
{
    const float CAT_BREAK_FORCE = 3000.0f;
    const float CAT_BREAK_TORQUE = 3000.0f;

    public CatState state = CatState.Projectile;

    public Transform[] paws;
    public Transform[] pawTips;

    public Transform[] pawStandingTransform;
    public Transform[] pawClingingTransform;

    public Transform tail;

	public bool shrinkMode = false;

    public Rigidbody rb;
    public AudioClip[] flyingSounds;
	public AudioClip[] attackSounds;

	public LayerMask clingLayerMask;
    public float pawClingRadius = 0.5f;
    private Quaternion originalTailLocalRotation;
    public AudioSource flyingAudioSource;

    private static Collider[] results = new Collider[64];
    private float randTail = 0.0f;
	private MaterialPropertyBlock mpb;
	public Gradient catColors;
	private MeshRenderer[] meshRenderers;

    // Start is called before the first frame update
    void Start()
    {
		mpb = new MaterialPropertyBlock();
		mpb.SetColor("_Color", catColors.Evaluate(Random.value));

		meshRenderers = GetComponentsInChildren<MeshRenderer>();

		PushMPB();

		randTail = Random.value * 10000.0f;
        originalTailLocalRotation = tail.localRotation;
        if (state == CatState.Projectile)
        {
            AudioClip clip = flyingSounds[Random.Range(0, flyingSounds.Length)];
            flyingAudioSource.clip = clip;
            flyingAudioSource.volume = Random.Range(0.3f, 0.5f);
            flyingAudioSource.pitch = Random.Range(0.8f, 1.2f);
            flyingAudioSource.Play();
        }
    }

    // Update is called once per frame
    void Update()
    {
        // move tail
		if (shrinkMode)
		{
			transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one * 0.01f, Time.deltaTime * 0.5f);
		}

        //tail.localRotation = originalTailLocalRotation * Quaternion.Euler(
        //    Mathf.Sin(Mathf.PerlinNoise(Time.deltaTime, randTail)*10.0f),
        //    Mathf.Sin(Mathf.PerlinNoise(Time.deltaTime, randTail+500.0f) * 10.0f),
        //    Mathf.Sin(Mathf.PerlinNoise(Time.deltaTime, randTail+1000.0f) * 10.0f)
        //    );
        tail.localRotation = originalTailLocalRotation * Quaternion.Euler(
            (Mathf.PerlinNoise(Time.time * 1f, randTail)-0.5f) * 60.0f,
            (Mathf.PerlinNoise(Time.time * 1f, randTail+1000.0f) - 0.5f) * 60.0f,
            (Mathf.PerlinNoise(Time.time * 1f, randTail + 2000.0f) - 0.5f) * 60.0f
            );
    }

	private void PushMPB()
	{
		for (int i = 0; i < meshRenderers.Length; i++)
		{
			meshRenderers[i].SetPropertyBlock(mpb);

		}
	}
    private void FixedUpdate()
    {
        switch(state)
        {
            case CatState.Projectile:
                UpdateProjectile();
                break;
            case CatState.Clinging:
                UpdateClinging();
                break;
            case CatState.Chasing:
                UpdateChasing();
                break;
			case CatState.Attacking:
				UpdateAttacking();
				break;
        }
    }

	private void UpdateAttacking()
	{
		FaceVictim();

		for (int i = 0; i < 4; i++)
		{

			paws[i].localRotation = Quaternion.Slerp(pawStandingTransform[i].localRotation, pawClingingTransform[i].localRotation, Mathf.Sin(Time.time * 14f + randTail + i*12345));
		}

		if (Time.time > nextAttackTime)
		{
			rb.velocity += Random.onUnitSphere * 5.0f + Vector3.up * 2.0f;
			state = CatState.Projectile;
			mpb.SetColor("_EmissionColor", normalEyeColor);

			for (int i = 0; i < 4; i++)
			{
				paws[i].GetComponent<Collider>().enabled = false;
			}

		}
	}

	private void OpenLegs()
    {
        for (int i = 0; i < 4; i++)
        {
            paws[i].localRotation = Quaternion.Slerp(paws[i].localRotation, pawClingingTransform[i].localRotation, 10.0f * Time.deltaTime);
        }

    }
    private void CloseLegs()
    {
        for (int i = 0; i < 4; i++)
        {
            paws[i].localRotation = Quaternion.Slerp(paws[i].localRotation, pawStandingTransform[i].localRotation, 10.0f * Time.deltaTime);
        }
    }
    private void UpdateProjectile()
    {
        OpenLegs();
		rb.sleepThreshold = 2.0f;
		rb.angularDrag = 0.0f;
		rb.drag = 0.0f;
    }
    private void UpdateClinging()
    {
        OpenLegs();
		rb.sleepThreshold = 2.0f;
		rb.angularDrag = 2.0f;
		rb.drag = 3.0f;
	}
	private void UpdateChasing()
    {
        CloseLegs();
    }



	private float nextAttackTime = 0.0f;
	private Transform attackTarget = null;
	private float attackSpin = 0.0f;
	public void Attack(Transform target)
	{
		//// first do damage
		//Damageable targetDamageable = target.GetComponent<Damageable>();
		//if (targetDamageable != null)
		//{
		//	targetDamageable.Damage(Time.deltaTime * 0.5f);
		//}

		if (state != CatState.Clinging)
			return;

		if (Time.time < nextAttackTime)
			return;


		attackTarget = target;

		FaceVictim();

		// remove all joints
		for (int i = 0; i < myJoints.Count; i++)
		{
			if (myJoints[i] == null) continue;

			myJoints[i].breakForce = 0.0f;
			myJoints[i].breakTorque = 0.0f;

			Destroy(myJoints[i]);
		}
		myJoints.Clear();
		nextAttackTime = Time.time + Random.Range(2.0f, 4.0f);

		state = CatState.Attacking;

		for (int i = 0; i < 4; i++)
		{
			paws[i].GetComponent<Collider>().enabled = true;
		}

		Vector3 directionToTarget = ((attackTarget.position + Vector3.up) - transform.position).normalized;
		//rb.velocity += directionToTarget * Random.Range(0.0f, 3.0f) + Vector3.up * Random.Range(0.0f, 3.0f);

		rb.drag = 0.0f;
		rb.angularDrag = 0.0f;

		AudioClip attackClip = attackSounds[Random.Range(0, attackSounds.Length)];
		ProcAudioSource.Play(attackClip, transform.position, Random.Range(0.5f, 7.0f), Random.Range(0.8f, 1.2f));

		mpb.SetColor("_EmissionColor", angryEyeColor);
		PushMPB();
	}

	public Color angryEyeColor;
	public Color normalEyeColor;

	private void FaceVictim()
	{
		// rotate to face player
		Vector3 directionToTarget = ((attackTarget.position + Vector3.up) - transform.position).normalized;

		attackSpin = Random.value * 360.0f;
		Quaternion targetRotation = Quaternion.FromToRotation(Vector3.down, directionToTarget) * Quaternion.AngleAxis(attackSpin, directionToTarget);
		rb.MoveRotation(Quaternion.Slerp(transform.rotation, targetRotation, Random.value * 10.0f * Time.deltaTime));
		//transform.rotation = 
	}

	private List<Joint> myJoints = new List<Joint>(8);

    private void OnCollisionEnter(Collision collision)
    {
        if (state == CatState.Projectile)
        {
            // check if it hits something good
            if (Layers.CheckLayerMask(collision.gameObject.layer, clingLayerMask))
            {
                // face the thing we collided with
                ContactPoint contactPoint = collision.GetContact(0);
                Vector3 normal = contactPoint.normal;

                transform.rotation = Quaternion.FromToRotation(Vector3.up, normal) * Quaternion.AngleAxis(Random.value * 360, normal); 
                
                state = CatState.Clinging;

                for (int i = 0; i < 4; i++)
                {
                    //Collider c = paws[i].GetComponent<Collider>();
                    //c.enabled = true;
                    Vector3 tip = pawTips[i].position;
                    int resultsCount = Physics.OverlapSphereNonAlloc(tip, pawClingRadius, results, clingLayerMask);
                    for(int j = 0; j < resultsCount; j++)
                    {
                        Rigidbody otherRb = results[j].attachedRigidbody;
                        if (otherRb != null && otherRb != rb)
                        {
                            FixedJoint joint = gameObject.AddComponent<FixedJoint>();
                            joint.connectedBody = otherRb;
                            joint.enableCollision = false;
                            joint.breakForce = CAT_BREAK_FORCE;
                            joint.breakTorque = CAT_BREAK_TORQUE;
							myJoints.Add(joint);
                        }

                    }
                }

                rb.velocity = Vector3.zero;
                rb.drag = 3.0f;
                rb.angularDrag = 5.0f;
            }
        }
    }
}
