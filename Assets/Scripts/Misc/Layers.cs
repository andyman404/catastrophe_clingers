﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Layers
{
	public static bool CheckLayerMask(int layer, LayerMask layerMask)
	{
		return ((layerMask.value & (1 << layer)) != 0);
	}

	public const int PLAYER = 8;
	public const int CATS = 9;
	public const int BUILDING = 10;
	public const int GROUND = 11;
	public const int STAIRS = 12;
}
