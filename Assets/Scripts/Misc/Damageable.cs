﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : MonoBehaviour
{
	public float health = 10.0f;
	public float maxHealth = 10.0f;

	public bool invulnerable = false;
	public bool dead = false;

	public string damageParticleSystemName;
	public int damageParticlesPerHealth = 10;
	public string deathParticleSystemName;
	public int deathParticleCount = 100;
	public Vector3 damageOffset;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private float damageParticlePool = 0.0f;
	
	public void Damage(float amount)
	{
		if (invulnerable || dead) return;

		Debug.Log("Damage: " + amount);
		health -= amount;

		damageParticlePool += amount * damageParticlesPerHealth;

		if (damageParticlePool > 1.0f)
		{
			GameObject g = Cached.Find(damageParticleSystemName);
			if (g != null)
			{
				ParticleSystem ps = g.GetComponent<ParticleSystem>();

				ps.transform.position = transform.position + damageOffset;
				int particles = Mathf.FloorToInt(damageParticlePool);
				damageParticlePool -= (float)particles;
				
				ps.Emit(Mathf.RoundToInt(particles));
			}
		}

		if (health <= 0.0f)
		{
			dead = true;
			health = 0.0f;
			Debug.Log("Dead");

			// death particles
			GameObject g = Cached.Find(deathParticleSystemName);
			if (g != null)
			{
				ParticleSystem ps = g.GetComponent<ParticleSystem>();
				ps.transform.position = transform.position + damageOffset;
				ps.Emit(deathParticleCount);
			}

			//SendMessage("DoDeath", SendMessageOptions.DontRequireReceiver);
		}

	}
}
